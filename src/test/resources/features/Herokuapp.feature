#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Alertas JavaScript
  Aprender a interactuar con alertas de tipo JavaScript en Selenium

  @CasoExitoso
  Scenario: Alertas JavaScript
    Given Ingresar al sitio de HeroKuapp
    When Ingrese al sitio JavaScript Alerts
    And Interactue con la opcion JavaScript Alerts
    And Interactue con la opcion JavaScript Confirm
    And Interactue con la opcion JavaScript Prompt
    Then Aprender a manejar alertas de tipo JavaScript
    
