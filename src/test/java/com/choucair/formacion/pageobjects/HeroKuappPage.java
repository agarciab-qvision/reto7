package com.choucair.formacion.pageobjects;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

import org.openqa.selenium.Alert;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://the-internet.herokuapp.com")

public class HeroKuappPage extends PageObject {
	
	

	// Cabecera de inicio del sitio
	@FindBy (xpath="//*[@id=\'content\']/h1")
    public WebElementFacade hdrInicio;
	
	// Enlace opción: JS Alerts
	@FindBy (xpath="//*[@id='content']/ul/li[25]/a")
    public WebElementFacade lnkJSAlerts;

	// Cabecera JS Alerts
	@FindBy (xpath="//*[@id=\'content\']/div/h3")
    public WebElementFacade hdrJSAlerts;
	
	// Botón JS Alert
	@FindBy (xpath="//*[@id=\'content\']/div/ul/li[1]/button")
    public WebElementFacade btnJSAlert;
	
	// Botón JS Confirm
	@FindBy (xpath="//*[@id=\'content\']/div/ul/li[2]/button")
	public WebElementFacade btnJSConfirm;
	
	// Botón JS Prompt
	@FindBy (xpath="//*[@id=\'content\']/div/ul/li[3]/button")
	public WebElementFacade btnJSPrompt;
	
	// Etiqueta Result
	@FindBy (xpath="//*[@id=\'result\']")
	public WebElementFacade lblConfirmacionMensajeResult;
	
	
	public void confirmarInicio() {
		
		String mensajeVerificacion = "Welcome to the-internet";
		String strMensaje = hdrInicio.getText();
		assertThat(strMensaje, containsString(mensajeVerificacion));
		
	}
	
	
	public void seleccionarJSAlerts() {
		
		lnkJSAlerts.click();
		
	}

	
	public void confirmarSeleccionJSAlerts() {
		
		String mensajeVerificacion = "JavaScript Alerts";
		String strMensaje = hdrJSAlerts.getText();
		assertThat(strMensaje, containsString(mensajeVerificacion));
		
	}


	public void ventanaJSAlert() throws InterruptedException {
		
		btnJSAlert.click();
		
		getDriver().manage().window().maximize();
		Alert alert = getDriver().switchTo().alert();
		Thread.sleep(3000);
		System.out.println("Alert Text:" + alert.getText());
		alert.accept();
		Thread.sleep(3000);
		getDriver().switchTo().defaultContent();
		
	}


	public void ventanaJSConfirm() throws InterruptedException {
		
		btnJSConfirm.click();
		
		getDriver().manage().window().maximize();
		Alert alert = getDriver().switchTo().alert();
		Thread.sleep(3000);
		System.out.println("Alert Text:" + alert.getText());
		alert.dismiss();
		Thread.sleep(3000);
		getDriver().switchTo().defaultContent();
		
	}


	public void ventanaJSPrompt() throws InterruptedException {

		btnJSPrompt.click();

		getDriver().manage().window().maximize();
		Alert alert = getDriver().switchTo().alert();
		Thread.sleep(3000);
		System.out.println("Alert Text:" + alert.getText());
		alert.sendKeys("Alexander Garcia Berrio");
		Thread.sleep(6000);
		alert.accept();
		Thread.sleep(6000);
		getDriver().switchTo().defaultContent();
		
	}


	public void confirmacionMensajeResult() {
		
		String mensajeVerificacion = "Alexander Garcia Berrio";
		String strMensaje = lblConfirmacionMensajeResult.getText();
		assertThat(strMensaje, containsString(mensajeVerificacion));
		
	}





		
}

