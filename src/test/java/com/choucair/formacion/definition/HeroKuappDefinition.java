package com.choucair.formacion.definition;

import com.choucair.formacion.steps.HeroKuappSteps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class HeroKuappDefinition {

	@Steps
	HeroKuappSteps heroKuappSteps;
	
	@Given("^Ingresar al sitio de HeroKuapp$")
	
	public void ingresar_al_sitio_de_HeroKuapp()  {
		
		heroKuappSteps.inicio();

	}

	@When("^Ingrese al sitio JavaScript Alerts$")
	public void ingrese_al_sitio_JavaScript_Alerts()  {
		
		heroKuappSteps.seleccionJSAlerts();
		
	}

	@When("^Interactue con la opcion JavaScript Alerts$")
	public void interactue_con_la_opcion_JavaScript_Alerts() throws InterruptedException  {
		
		heroKuappSteps.abrirVentanaJSAlert();
		
	}

	@When("^Interactue con la opcion JavaScript Confirm$")
	public void interactue_con_la_opcion_JavaScript_Confirm() throws InterruptedException  {
		
		heroKuappSteps.abrirVentanaJSConfirm();
		
	}

	@When("^Interactue con la opcion JavaScript Prompt$")
	public void interactue_con_la_opcion_JavaScript_Prompt() throws InterruptedException {
		
		heroKuappSteps.abrirVentanaJSPrompt();
		
	}

	@Then("^Aprender a manejar alertas de tipo JavaScript$")
	public void aprender_a_manejar_alertas_de_tipo_JavaScript()  {
		
		heroKuappSteps.leccionAprendida();
	
	}
}
