package com.choucair.formacion.steps;

import com.choucair.formacion.pageobjects.HeroKuappPage;

public class HeroKuappSteps {

	HeroKuappPage heroKuappPage;

	public void seleccionJSAlerts() {
		
		heroKuappPage.seleccionarJSAlerts();
		heroKuappPage.confirmarSeleccionJSAlerts();
		
	}

	public void inicio() {
		
		heroKuappPage.open();
		heroKuappPage.confirmarInicio();
		
	}

	public void abrirVentanaJSAlert() throws InterruptedException {
		
		heroKuappPage.ventanaJSAlert();
		
	}

	public void abrirVentanaJSConfirm() throws InterruptedException {
		
		heroKuappPage.ventanaJSConfirm();
		
	}

	public void abrirVentanaJSPrompt() throws InterruptedException {
		heroKuappPage.ventanaJSPrompt();
		
	}

	public void leccionAprendida() {
		
		heroKuappPage.confirmacionMensajeResult();
		
	}


}
